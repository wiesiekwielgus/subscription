<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SubscriberRepository")
 */
class Subscriber implements SubscriberInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(name="email",type="string",length=255, unique=true)
     */
    protected $email;

    /**
     * @ORM\Column(name="confirmed",type="boolean")
     */
    protected $confirmed = false;

    public function getId(): int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): SubscriberInterface
    {
        $this->email = $email;
        return $this;
    }

    public function getConfirmed(): bool
    {
        return $this->confirmed;
    }

    public function setConfirmed(bool $confirmed): SubscriberInterface
    {
        $this->confirmed = $confirmed;
        return $this;
    }
}