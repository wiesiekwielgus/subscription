<?php
/**
 * Created by PhpStorm.
 * User: wwielgus
 * Date: 23.01.18
 * Time: 11:12
 */

namespace App\Entity;

interface SubscriberInterface
{
    public function getId(): int;

    public function getEmail(): ?string;

    public function setEmail(string $email): SubscriberInterface;

    public function getConfirmed(): bool;

    public function setConfirmed(bool $confirmed): SubscriberInterface;
}