<?php

namespace App\Repository;

use App\Entity\Subscriber;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class SubscriberRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Subscriber::class);
    }

    public function findConfirmedSubscribers()
    {
        return $this->createQueryBuilder('s')
            ->where('s.confirmed = true')
            ->orderBy('s.email', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}
