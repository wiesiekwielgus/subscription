<?php

namespace App\Utils\MessageGenerator;

interface MessageGeneratorInterface
{
    public function create(string $title, string $sender, string $recipient, string $template): \Swift_Message;
}