<?php

namespace App\Utils\MessageGenerator;

class MessageGenerator implements MessageGeneratorInterface
{
    /** @var \Twig_Environment */
    protected $twig;

    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    public function create(string $title, string $sender, string $recipient, string $template): \Swift_Message
    {
        $message = (new \Swift_Message($title))
            ->setFrom($sender)
            ->setTo($recipient)
            ->setBody(
                $this->twig->render(
                    $template,
                    array('recipient' => $recipient)
                ),
                'text/html'
            )
        ;

        return $message;
    }
}