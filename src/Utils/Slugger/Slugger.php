<?php

namespace App\Utils\Slugger;

use App\Utils\Slugger\SluggerException;

class Slugger implements SluggerInterface
{
    public function slugify(string $text): string
    {
        $text = preg_replace('~[^\pL]+~u', '', $text);
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = trim($text, '\s-');
        $text = preg_replace('~-+~', '-', $text);
        $text = strtolower($text);

        if (empty($text) || $text === '-') {
            throw new SluggerException('Failed to slugify string "'.$text.'"');
        }

        return $text;
    }
}