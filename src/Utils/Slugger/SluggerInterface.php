<?php

namespace App\Utils\Slugger;

interface SluggerInterface
{
    public function slugify(string $text): string;
}