<?php

namespace App\Utils\MessageSender;

use App\Utils\MessageGenerator\MessageGeneratorInterface;

class MessageSender implements MessageSenderInterface
{
    /** @var \Swift_Mailer */
    protected $mailer;

    /** @var MessageGeneratorInterface */
    protected $messageGenerator;

    public function __construct(\Swift_Mailer $mailer, MessageGeneratorInterface $messageGenerator)
    {
        $this->mailer = $mailer;
        $this->messageGenerator = $messageGenerator;
    }

    public function send(string $title, string $sender, string $recipient, string $template): void
    {
        $message = $this->messageGenerator->create($title, $sender, $recipient, $template);
        $this->mailer->send($message);
    }
}