<?php

namespace App\Utils\MessageSender;

interface MessageSenderInterface
{
    public function send(string $title, string $sender, string $recipient, string $template): void;
}