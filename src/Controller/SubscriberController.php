<?php

namespace App\Controller;

use App\Entity\Subscriber;
use App\Entity\SubscriberInterface;
use App\Form\SubscriberType;
use App\Manager\Subscriber\ConfirmedSubscriberExistsException;
use App\Manager\Subscriber\NotConfirmedSubscriberExistsException;
use App\Manager\Subscriber\SubscriberManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route(path="/subscriber")
 */
class SubscriberController extends Controller
{
    /**
     * @Route("/subscribe", name="subscriber_subscribe")
     */
    public function subscribe(Request $request)
    {
        $subscriber = new Subscriber();
        $form = $this->createForm(SubscriberType::class,$subscriber);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            /** @var SubscriberManagerInterface $subscriberManager */
            $subscriberManager = $this->container->get(SubscriberManagerInterface::class);
            try {
                $subscriberManager->add($subscriber);
            } catch (ConfirmedSubscriberExistsException $exception) {
                return $this->render('subscriber/view/confirmedSubscriberExists.html.twig');
            } catch (NotConfirmedSubscriberExistsException $exception) {
                return $this->render('subscriber/view/notConfirmedSubscriberExists.html.twig');
            }

            return $this->render('subscriber/view/subscribed.html.twig');
        }
        return $this->render('subscriber/view/subscribe.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/confirm/{email}", name="subscriber_confirm")
     * @ParamConverter("subscriber", class="App\Entity\Subscriber")
     */
    public function confirm(SubscriberInterface $subscriber)
    {
        /** @var SubscriberManagerInterface $subscriberManager */
        $subscriberManager = $this->container->get(SubscriberManagerInterface::class);
        $subscriberManager->confirm($subscriber);

        return $this->render('subscriber/view/confirm.html.twig');
    }

    /**
     * @Route("/unsubscribe/{email}", name="subscriber_unsubscribe")
     * @ParamConverter("subscriber", class="App\Entity\Subscriber")
     */
    public function unsubscribe(SubscriberInterface $subscriber)
    {
        /** @var SubscriberManagerInterface $subscriberManager */
        $subscriberManager = $this->container->get(SubscriberManagerInterface::class);
        $subscriberManager->remove($subscriber);

        return $this->render('subscriber/view/unsubscribe.html.twig');
    }
}
