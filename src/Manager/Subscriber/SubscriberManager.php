<?php

namespace App\Manager\Subscriber;

use App\Entity\Subscriber;
use App\Entity\SubscriberInterface;
use App\Utils\MessageSender\MessageSender;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;

class SubscriberManager implements SubscriberManagerInterface
{
    /** @var EntityManagerInterface */
    protected $entityManager;

    /** @var MessageSender */
    protected $messageSender;

    /** @var string */
    protected $sender;

    public function __construct(EntityManagerInterface $entityManager, MessageSender $messageSender, string $sender)
    {
        $this->entityManager = $entityManager;
        $this->messageSender = $messageSender;
        $this->sender = $sender;
    }

    public function add(SubscriberInterface $subscriber): void
    {
        try {
            $this->persistEntity($subscriber);
        } catch (UniqueConstraintViolationException $exception) {
            /** @var SubscriberInterface $subscriber */
            $subscriber = $this->entityManager->getRepository(Subscriber::class)->findOneByEmail($subscriber->getEmail());
            if($subscriber->getConfirmed()) {
                throw new ConfirmedSubscriberExistsException();
            } else {
                $this->sendSubscriptionConfirmationMessage($subscriber->getEmail());
                throw new NotConfirmedSubscriberExistsException();
            }
        }
        $this->sendSubscriptionConfirmationMessage($subscriber->getEmail());
    }

    public function confirm(SubscriberInterface $subscriber): void
    {
        $subscriber->setConfirmed(true);
        $this->persistEntity($subscriber);
        $this->sendSubscriptionConfirmedMessage($subscriber->getEmail());
    }

    public function remove(SubscriberInterface $subscriber): void
    {
        $this->removeEntity($subscriber);
        $this->sendSubscriptionCancelledMessage($subscriber->getEmail());
    }

    protected function persistEntity(SubscriberInterface $subscriber): void
    {
        $this->entityManager->persist($subscriber);
        $this->entityManager->flush();
    }

    protected function removeEntity(SubscriberInterface $subscriber): void
    {
        $this->entityManager->remove($subscriber);
        $this->entityManager->flush();
    }

    protected function sendSubscriptionConfirmationMessage(string $recipient): void
    {
        $this->messageSender->send('Potwierdź subskrypcję', $this->sender, $recipient, 'subscriber/message/confirmation.html.twig');
    }

    protected function sendSubscriptionConfirmedMessage(string $recipient): void
    {
        $this->messageSender->send('Potwierdzono subskrypcję', $this->sender, $recipient, 'subscriber/message/confirmed.html.twig');
    }

    protected function sendSubscriptionCancelledMessage(string $recipient): void
    {
        $this->messageSender->send('Anulowano subskrypcję', $this->sender, $recipient, 'subscriber/message/removed.html.twig');
    }
}