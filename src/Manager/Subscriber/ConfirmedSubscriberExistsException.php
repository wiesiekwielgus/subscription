<?php

namespace App\Manager\Subscriber;

class ConfirmedSubscriberExistsException extends \Exception
{
}