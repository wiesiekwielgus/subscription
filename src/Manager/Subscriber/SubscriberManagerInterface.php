<?php

namespace App\Manager\Subscriber;

use App\Entity\SubscriberInterface;

interface SubscriberManagerInterface
{
    public function add(SubscriberInterface $subscriber): void;

    public function confirm(SubscriberInterface $subscriber): void;

    public function remove(SubscriberInterface $subscriber): void;
}